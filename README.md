# IBAN validator
Author: André Gomes (andre.gomes@finologee.com)

## Overview
Simple REST API that validates IBAN numbers using the library 
[Iban4j](https://github.com/arturmkrtchyan/iban4j).

## Configuration
### Prerequisites
- [JDK13](https://jdk.java.net/13/) installed and JAVA_HOME set
- [Maven](https://maven.apache.org/download.cgi) installed and MAVEN_HOME set
- [Docker](https://docs.docker.com/install/) installed and running
- (Optional) [Postman](https://www.getpostman.com/)

## Installation
Run `build.sh` to:
1. Perform Maven Build
2. Generate Docker image
3. Push image to Docker Hub

When the script finishes there will be an image named atxgomes/iban-validator:latest both on
local repository and Docker Hub.

## Operation
To start the service execute `docker-compose up`. See `docker-compose.yml` to obtain the port number.

To validate that the service is running check the health endpoint at `http://localhost:[port]/actuator/health`.

For a few example how to use this API, import collection at `./src/postman` and run it.

## Troubleshooting
**1.The requests in the Postman collection do not work**

Make sure the service is running. If the service is running, make sure Postman is using the correct
port numbers. By default, it assumes the service is available at `http://localhost:8083`.

**2.The image is not pushed to Docker Hub.**

The script is using the authors' credentials to push image to Docker Hub. Contact the author 
if you want to push to his repository. Otherwise, update the image name and Docker username
and push to your own repo.

The script is reading the Docker password from a file in same directory named `docker-login.txt`.
