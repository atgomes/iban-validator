#!/usr/bin/env bash
echo 'Maven build'
mvn clean package -f ./pom.xml

echo 'Building Docker image'

docker build --no-cache -t atxgomes/iban-validator:latest .

echo 'Docker login'

cat ./docker-login.txt | docker login -u atxgomes --password-stdin

echo 'Pushing Docker image to Docker Hub'

docker push atxgomes/iban-validator:latest
