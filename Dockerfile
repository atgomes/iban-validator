FROM adoptopenjdk/openjdk13-openj9

LABEL maintainer="andre.gomes@finologee.com"

RUN groupadd --system appuser && useradd --system --gid appuser --shell /bin/bash appuser

WORKDIR /home/appuser

ENV JAVA_TOOL_OPTIONS -XX:+UseContainerSupport -XX:MaxRAMPercentage=85

ARG DEPENDENCY=target/dependency

COPY ${DEPENDENCY}/BOOT-INF/lib app/lib
COPY ${DEPENDENCY}/META-INF app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes app

RUN chown -R appuser:appuser app

USER appuser

EXPOSE 8080/tcp

ENTRYPOINT ["java","-classpath","app:app/lib/*","com.finologee.ibanvalidator.IbanvalidatorApplication"]