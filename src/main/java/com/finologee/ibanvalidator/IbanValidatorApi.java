package com.finologee.ibanvalidator;

import org.iban4j.Iban;
import org.iban4j.IbanFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/iban")
public class IbanValidatorApi
{

  private static final Logger log = LoggerFactory.getLogger(IbanValidatorApi.class);

  @PostMapping()
  public ResponseEntity<Object> validateIban(@RequestHeader("x-client-name") String clientName,
                                                 @RequestParam(value = "format", required = false) Boolean validateFormat,
                                                 @RequestBody Request request)
  {

    log.info("Validating IBAN {} for client {}", request.getIban(), clientName);

    String ibanStr = request.getIban();

    if (StringUtils.isEmpty(ibanStr)) {
      return ResponseEntity.badRequest().body("Mandatory IBAN number is missing");
    }

    try {
      Iban iban;
      if (validateFormat != null && validateFormat) {
        iban = Iban.valueOf(ibanStr, IbanFormat.Default);
      }
      else {
        iban = Iban.valueOf(StringUtils.trimAllWhitespace(ibanStr));
      }

      log.info("IBAN {} is valid", ibanStr);

      return ResponseEntity.ok(new IbanResult(true,
          "Valid IBAN",
          iban.getAccountNumber(),
          iban.getBankCode(),
          iban.getCountryCode().name()));

    } catch (Exception ex) {
      log.error("IBAN {} is invalid. Reason: {}", ibanStr, ex.getLocalizedMessage());
      return ResponseEntity.ok().body(new IbanResult(false, ex.getLocalizedMessage()));
    }
  }
}
