package com.finologee.ibanvalidator;

public class Request
{
  private String iban;

  public String getIban() {
    return iban;
  }

  public void setIban(final String iban) {
    this.iban = iban;
  }
}
