package com.finologee.ibanvalidator;

public class IbanResult
{
  private boolean valid;

  private String message;

  private String accountNumber;

  private String bankCode;

  private String countryCode;

  public IbanResult(final boolean valid, final String message) {
    this(valid, message, null, null, null);
  }

  public IbanResult(final boolean valid, final String message, final String accountNumber, final String bankCode, final String countryCode) {
    this.valid = valid;
    this.message = message;
    this.accountNumber = accountNumber;
    this.bankCode = bankCode;
    this.countryCode = countryCode;
  }

  public boolean isValid() {
    return valid;
  }

  public void setValid(final boolean valid) {
    this.valid = valid;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(final String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getBankCode() {
    return bankCode;
  }

  public void setBankCode(final String bankCode) {
    this.bankCode = bankCode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(final String countryCode) {
    this.countryCode = countryCode;
  }
}
