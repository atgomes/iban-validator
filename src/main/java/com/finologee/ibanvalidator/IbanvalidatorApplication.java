package com.finologee.ibanvalidator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbanvalidatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbanvalidatorApplication.class, args);
	}

}
